import { createContext } from 'react';
import { IData } from '../../interfaces';

/**
 * I create my context to have my favorites and my functions that will
 * will help me to remove or add information to the status
 */

interface ContextProps {
    favorites: IData[];

    // Methods
    addFavorite: (favorite: IData) => void;
    removeFavorite: (favorite: IData) => void;
}

 export const FavoriteContext = createContext({} as ContextProps);