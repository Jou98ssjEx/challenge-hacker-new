import { FavoriteState } from '.';
import { IData } from '../../interfaces/responseApi';

/**
 * This function helps me to change the state and to render the component with the requested information.
 * render the component with the information requested, in them
 * I have the functions to add and remove favorites.
 */

type FavoriteActionType = 
   | { type: '[Favorite] - Add', payload: IData }
   | { type: '[Favorite] - Remove', payload: IData }

export const favoriteReducer  = ( state: FavoriteState, action: FavoriteActionType ): FavoriteState => {
   switch (action.type) {

       case '[Favorite] - Add':
           return {
               ...state,
               favorites: [ ...state.favorites, action.payload ]
           }         
       case '[Favorite] - Remove':
           return {
               ...state,
               favorites: state.favorites.filter( fav => fav.id !== action.payload.id )
           }  
           
       default:
          return state;
   }
} 