import { FC, useReducer, ReactNode, useEffect } from 'react';
import { IData } from '../../interfaces';
import { FavoriteContext, favoriteReducer } from './';


/**
 * I create my Favorites Provider to at my highest point of
 * my application, provide this information
 */

export interface FavoriteState {
   favorites: IData[];
}

const Favorite_INITIAL_STATE: FavoriteState = {
   favorites: [],
}

/**
 * This Provider is a component where all the children will pass through
 * that you have 
 */

interface Props  {
   children?: ReactNode;
}

/**
 * 
 * I create a function that allows me to extract the state of the localStorage
 * and if it doesn't exist I give it the default values that were defined.
 * 
 */

const dataLocal = () => {
   const s = localStorage.getItem('favorites');
   if(!s){
      return Favorite_INITIAL_STATE;
   }

   return JSON.parse(s);
}


export const FavoriteProvider: FC <Props> = ( {children} ) => {

   const init = dataLocal();
   // console.log(init())

  const [state, dispatch] = useReducer(favoriteReducer, init );

// Saved favorites
  useEffect(() => {
   localStorage.setItem('favorites', JSON.stringify(state));
  }, [state])

// add favorites
const addFavorite = ( favorite: IData )=>{

   // console.log({favorite});
   dispatch({
      type:'[Favorite] - Add',
      payload: favorite,
   })
}

// delete favorites
const removeFavorite = ( favorite: IData )=>{

   dispatch({
      type:'[Favorite] - Remove',
      payload: favorite,
   })
}

  return(
     <FavoriteContext.Provider value={{
            ...state,

            // Methods
            addFavorite,
            removeFavorite
         }}
     >
        { children }
     </FavoriteContext.Provider>
  )

}