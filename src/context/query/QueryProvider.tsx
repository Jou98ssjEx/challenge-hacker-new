import { FC, useReducer, ReactNode, useEffect } from 'react';
import { callEndPoint } from '../../helper';

import { useHackerNew } from '../../hooks';
import { IData } from '../../interfaces';
import { optionQuery } from '../../utils';
import { QueryContext, queryReducer } from './';

/**
 * I create query Provider to at my highest point of
 * my application, provide this information
 */


export interface QueryState {
   optionQuery: optionQuery;
   page: string;
   dataCartNew: IData[];

   // isFavorite: IData[];

}

const Query_INITIAL_STATE: QueryState = {
   optionQuery: 'All',
   page: '1',
   dataCartNew: [],

   // isFavorite: [],

}

/**
 * This Provider is a component where all the children will pass through
 * that you have 
 */

interface Props  {
   children?: ReactNode;
}


/**
 * 
 * I create a function that allows me to extract the state of the localStorage
 * and if it doesn't exist I give it the default values that were defined.
 * 
 */

const dataLocal = () => {
   const s = localStorage.getItem('state');
   if(!s){
      return Query_INITIAL_STATE;
   }

   return JSON.parse(s);
}

export const QueryProvider: FC <Props> = ( {children} ) => {

   const init = dataLocal();

   const [state, dispatch] = useReducer(queryReducer, init );

   /**
    * 
    */

   /**
    * I use this custom hook to call my endpoint 
    * with the information it requires directly from my state
    */
   const { cardNew } = useHackerNew(state.optionQuery, state.page);


   /**
    * This effect keeps me from losing the change of information 
    * of my hook
    */
   useEffect(() => {

      if(cardNew.length>0){
         dispatch({type:'[CardNew] - LoadCard', payload: cardNew});
      }

   }, [cardNew])

   /**
    * This status is pending on the favorites, backups, etc.
    * this information in the local Storage because when changing
    * of page my information arrangement changes, that leaves a 
    * pending for the deletion of favorites in the favorites view.
    * If you have a data that does not match in the * new arg, you can't delete it in the new arg.
    * new array you can't delete it.
    */

   // useEffect(() => {

   //    if(state.dataCartNew.length > 1){
   //       let b = JSON.parse(localStorage.getItem('olDavorites')!) ||[]

   //       let arr = state.dataCartNew.filter( d => d.isFavorite === true);

   //       let unir = [...arr, ...b];

   //       let result = unir.filter( (item, index, self) => index === self.findIndex( (t)=> (t.id === item.id)));

   //       // console.log(result);
    
   //       localStorage.setItem('favorites', JSON.stringify(result));

   //       dispatch({type: '[Favorite] - arrFavorite', payload: result});

   //    }

   // }, [state.dataCartNew])


/**
 * 
 * Methods that allow me to make state changes
 * from anywhere in my application
 */

  const changeQueryOption = ( query: optionQuery) => {

    dispatch({
        type:'[Query] - UpdateQuery',
        payload: query
    })

  }

  const changePage = ( page: string) => {

    dispatch({
        type:'[Page] - UpdatePage',
        payload: page
    })

  }

  const loadDataCardNew = async ( ) => {


     const resp = await callEndPoint(state.optionQuery, state.page);

     // save local
     localStorage.setItem('data', JSON.stringify(resp));

     if(resp.info.length > 0){
        console.log(resp);
      dispatch({type:'[CardNew] - UpdateCard', payload: resp.info})
      return;
   }



  }

// cambio estado en data de este state
  const changeFavorite = (data: IData ) => {

     if (!data.isFavorite){
         data.isFavorite = true;
      }else{
         data.isFavorite = false;
      }
      
      dispatch({type:'[Favorite] - isFavorite', payload: data})


  }

//   const allFavorite = ( data: IData[] ) => {
//    dispatch({type: '[Favorite] - arrFavorite', payload: data});

//   }

  return(
     <QueryContext.Provider value={{
               ...state,

            //  Methods
            changeQueryOption,

            loadDataCardNew,

            changeFavorite,

            changePage,

            // allFavorite,

         }}
        >
        { children }
     </QueryContext.Provider>
  )

}