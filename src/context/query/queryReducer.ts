import { IData } from '../../interfaces';
import { optionQuery } from '../../utils';
import { QueryState } from './';

/**
 * This function helps me to change the state and to render the component with the requested information.
 * render the component with the requested information, in them
 * I have the functions load the data, change the page and the query,
 * store the favorites 
 */

type QueryActionType = 
   | { type: '[Query] - UpdateQuery', payload: optionQuery}
   | { type: '[Page] - UpdatePage', payload: string}
   | { type: '[CardNew] - LoadCard', payload: IData[]}
   | { type: '[CardNew] - UpdateCard', payload: IData[]}
   | { type: '[Favorite] - isFavorite', payload: IData }
   // | { type: '[Favorite] - arrFavorite', payload: IData[] }

export const queryReducer  = ( state: QueryState, action: QueryActionType ): QueryState => {
   switch (action.type) {

       case '[Query] - UpdateQuery':
           return {
               ...state,
                optionQuery: action.payload,
           }   
           
       case '[Page] - UpdatePage':
           return {
               ...state,
                page: action.payload,
           }   
           
         case '[CardNew] - LoadCard':
            return{
               ...state,
               dataCartNew: action.payload
            }
         case '[CardNew] - UpdateCard':
            return{
               ...state,
               dataCartNew: action.payload,
            }

         case '[Favorite] - isFavorite':
            return{
               ...state,
               dataCartNew: [ ...state.dataCartNew.map( data => data.id === action.payload.id ? action.payload :data)],
            
            }

         // case '[Favorite] - arrFavorite':
         //    return{
         //       ...state,
         //       isFavorite: [ ...action.payload ]
            
         //    }

       default:
          return state;
   }
} 