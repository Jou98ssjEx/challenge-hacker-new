import { createContext } from 'react';
import { IData } from '../../interfaces';
import { optionQuery } from '../../utils';

/**
 * This context will help me to maintain my status and to be able to change
 * its properties
 */

interface ContextProps {
    optionQuery: optionQuery;

    page: string;

    dataCartNew: IData[];

    // isFavorite: IData[];

    // Methods
    changeQueryOption: (query: optionQuery) => void

    loadDataCardNew: () => void

    changeFavorite: (data: IData) => void
    
    changePage: (page: string) => void
    // allFavorite: (data: IData[]) => void
}

 export const QueryContext = createContext({} as ContextProps);