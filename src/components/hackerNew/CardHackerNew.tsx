import { useContext, FC, useEffect } from 'react';

import { QueryContext } from '../../context';

import { CardItem } from './CardItem';

import { IData } from '../../interfaces';

import './CardHackerNewModule.css'


/**
 * Denne komponent vil gøre det muligt at vise alle oplysninger i form af kort.
 * den vil have en valgfri favorit-egenskab for at genbruge komponenten
 * 
 * @param ?favorites
 * 
 */

interface Props {
  favorites ?: IData[];
}


export const CardHackerNew: FC<Props> = ({ favorites }) => {

  /**
   * I use the context to bring the information and visualize it.
   * every time I have a change my context will always bring me the data
   * updated
   */

  const state = useContext(QueryContext);
  
  /**
   * I use an effect to locally save the information when performing
   * a change in the state
   */

  useEffect(() => {
   localStorage.setItem('state', JSON.stringify(state))
  }, [state])
  
  /**
   * 
   * If my favorite CardItem component exists, it will be rendered,
   * if it doesn't take whatever is in the state and that's what will be 
   * will be displayed 
   */

  return (
    <div className='container cards'>

      {
        favorites ? (
          favorites.map((i) =>(
            <CardItem 
              key={i.id} info={i}  
            />
          ))
        ) : (

          state.dataCartNew.map( (i) => (
            <CardItem 
              key={i.id} info={i}  
            />
          ))
        )
      }
    </div>
  )
}
