import { FC, useState, useContext } from 'react';

import moment from "moment";

import { QueryContext, FavoriteContext } from '../../context';

import { IData } from "../../interfaces"


/**
 * This component will render each object in the form of a card.
 * of type IData that it receives
 * 
 * @param info
 */

interface Props {
    info: IData;
}

export const CardItem: FC<Props> = ({info}) => {

    /**
     * I use my functions of my context to make the change of the state of the object.
     * on the object, this will add or remove it from the favorites.
     */

    const { changeFavorite,  } = useContext(QueryContext);
    // const { changeFavorite, isFavorite, allFavorite } = useContext(QueryContext);

    const {addFavorite, removeFavorite} = useContext(FavoriteContext);

    // Desestructuro info
    const {author, created_at, story_title, story_url } = info;

    /**
     * I use a change of state to display the favorite icon.
     * the realation true === 3 and false === 2 is made, the boolean value is
     * to change my state, and the numerical value is to change the image.
     * in what the component renders
     */
    const [, setFavoriteImg] = useState(2);

    /**
     * I realize a function for the change of image with the relation
     * above mentioned
     */
    const changeImg = () => {

        if (info.isFavorite){
            setFavoriteImg(3)
            // info.isFavorite = true;
            console.log('add')

            changeFavorite(info);
            removeFavorite(info);
            
        } else{
            
            // info.isFavorite = false;
            
            console.log('remove');
            addFavorite(info);
            // const fav:IData[] = JSON.parse(localStorage.getItem('favorites')!) || [];

            // fav.filter( d => d.id !== favorite.id);
            
            
            // let a = isFavorite.filter( d=> d.id === info.id )
            // console.log({a});
            // localStorage.setItem('favorites', JSON.stringify(isFavorite));
            // localStorage.setItem('olDavorites', JSON.stringify(isFavorite));
            // allFavorite(isFavorite)
            
            setFavoriteImg(2)
            changeFavorite(info);


        }

    }
    
  return (
    <div
        className='card'
    >
        <a className='card__content'
            href={story_url!}
            target={'_blank'}
            rel={"noreferrer"}
        >

                <div className='card__body'>
                    <img className='card__body__icon' src='/assets/iconmonstr-time-2.svg' alt='Time'/>
                    <span className='card__body__textTime'>
                        {moment(created_at).fromNow()} by {author}
                    </span>
                </div>
                <span className='card__body__text' >
                    {story_title}
                </span>
        </a>

        <div className='card__icon'
            onClick={changeImg}
        >
            <img className='' src={`/assets/iconmonstr-favorite-${ info.isFavorite ? 3: 2 }.svg`} alt="Like" />
        </div>
    </div>
  )
}
