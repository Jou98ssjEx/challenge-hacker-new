import { FC, useContext } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';

import queryString from 'query-string';

import { QueryContext } from '../../context/query/QueryContext';

/**
 * This component receives a static number of pages, which it
 * renders that number of items
 * 
 * @param pageNumber
 */

interface Props{
    pageNumber: number
}


export const PaginationNumber: FC <Props> = ({pageNumber}) => {

    /**
     * From my context I have my function to change the page which has
     * as argument the number of the page in which it is located
     * for the navigation I use my hook, and to know where I am
     * I use the location another hook, I use a library to extract the query
     * and page of my url, so that I can specify to my component which data and styles to apply
     * and styles to apply
     */

    const { changePage } = useContext(QueryContext);

    const navigate = useNavigate();

    const location = useLocation();

    const { query, page } = queryString.parse(  location.search);

    /**
     * I create my function to know which page I am on
     */
    const onPagination = ( ) => {
        navigate(`?query=${ !query ? 'all': query }&page=${pageNumber ? pageNumber+1: '1'}`);
        const a = pageNumber +1;
        changePage(a.toString())
    }

  return (
    <div  className={`${ (pageNumber+1) === Number(page) ?`rectanglePagination active`:`rectanglePagination`}`} onClick={onPagination}>
        <div className='contentNumber'>
            <div className={`${ (pageNumber+1) === Number(page) ?`activeNumberItem`:`numberItem`}`}> 
                <span> {pageNumber+1} </span>
            </div>
        </div>
    </div>
  )
}
