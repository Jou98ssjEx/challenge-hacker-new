import { FC } from 'react';
import {useNavigate} from 'react-router-dom'

import './MenuModule.css';

/**
 * This component will be in charge of changing the view of the called and favorite cards.
 * that are called and those that are favorite, it uses a change of state to show this change.
 * to show this change.
 * 
 * @param changeScreen
 * @param setChangeScreen
 */

interface Props {
    changeScreen: boolean;
    setChangeScreen: (status: boolean) => void;
 }

export const Menu: FC<Props> = ({ changeScreen, setChangeScreen }) => {

    // I use this hook to be able to send query and browse
    const navigate = useNavigate()


    // I extract the state that is in my localStorage
    let data: any = localStorage.getItem('state');

    /**
     * I create a function that allows me to change the view.
     * I have the view ALL === true, MYFAVES === false, of my state
     * I have the properties query and page so that when changing I do not loose
     * the state in which it is
     */

    const onClickPage = ( ) => {
        setChangeScreen(!changeScreen);

        if(changeScreen){
            navigate(`/`)
        } else {
            data = JSON.parse(data!);
            navigate(`/?query=${data.optionQuery.toLowerCase()}&page=${data.page}`);
        }

        // I save the status of the menu
        localStorage.setItem('menu', JSON.stringify(!changeScreen));

    }

  return (
    <div
        className='container__btn'
    >
        {/* BNT ALL AND MY FAVES */}
        <div className={ changeScreen ? `box__btn_active`: `box__btn`}
            onClick={ onClickPage }
        >
            
            <span className="">
                All
            </span>
        </div>

        <div className={ !changeScreen ? `box__btn_active`: `box__btn`}
            onClick={ onClickPage }
        >
            <span className="">
                My faves
            </span>
        </div>
    </div>
  )
}
