import './NavbarModule.css';

/**
 * 
 * This component only reflects the head of the view.
 * 
 */

export const Navbar = () => {
  return (
    <div
      className='navbar'
    >
      <img className='container logoimg' src="/assets/hacker-news.svg" alt="" />

    </div>
  )
}
