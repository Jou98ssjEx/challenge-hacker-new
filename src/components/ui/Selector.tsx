import { useState } from 'react';

import { selectItemData } from '../../utils';
import { SelectItem } from './SelectItem';

import './SelectorModule.css'

/**
 * 
 * This component is a container to be able to filter the information that the 
 * information that it asks the endpoint for.
 * 
 */


export const Selector = () => {

    /**
     * I execute a change of state to render the option I will choose.
     */

    const [viewOptionSelect, setViewOptionSelect] = useState(false);

    const onSelectChoose = () => {
        setViewOptionSelect(!viewOptionSelect);
    }


  return (
      <>
        <div
         className='container container__select'
         onClick={ onSelectChoose}
        >
            {/* CBX */}
            <div className=" rectangle__select">
                <span className="text__select">
                    Select your news
                </span>
                <div className='icon__select'>
                    <div className="mask__select"></div>
                </div>
            </div>

        </div>

        {/* selector */}
        <div
            className='content__select'
            style={{
                display: !viewOptionSelect ? 'none': 'flex'
            }}            
        >

            {
                selectItemData.items.map( item => (
                    <SelectItem 
                        key={item.name} 
                        viewOptionSelect={viewOptionSelect} 
                        setViewOptionSelect={setViewOptionSelect} 
                        data={item}
                    />
                ))
            }

        </div>
     </>
  )

}
