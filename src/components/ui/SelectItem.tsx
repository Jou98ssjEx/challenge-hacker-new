import { FC, useContext } from 'react';
import {useNavigate, useLocation} from 'react-router-dom'

import queryString from 'query-string'

import { QueryContext } from '../../context';

import { ISelectData } from "../../utils"


/**
 * This component allows me to know which filter to apply when I call
 * the endpoint that brings the information, the parameters that I receive
 * are the following
 * 
 * @param viewOptionSelect
 * @param setViewOptionSelect
 * @param data
 */

interface Props{
    viewOptionSelect: boolean
    setViewOptionSelect: (status: boolean) => void;
    data: ISelectData;
}


export const SelectItem: FC<Props> = ({ data, setViewOptionSelect, viewOptionSelect }) => {

    /**
     * In my context I have the state of the query and a function to change it.
     * to change it, I use my hooks to know which page I'm on
     * so I can filter that page
     */

    const {optionQuery, changeQueryOption,  } = useContext(QueryContext);

    const navigate = useNavigate();

    const location = useLocation();

    const { page } = queryString.parse(  location.search);

    /**
     * I create my function that allows me to filter
     */
   
    const handleClick =async ( ) => {
        changeQueryOption(data.name);
        setViewOptionSelect(!viewOptionSelect);
        navigate(`?query=${ data.name.toLowerCase()}&page=${page ? page: '1'}`);
    }


  return (
    <div 
        className={ optionQuery === data.name ? `activeSelectItem` :`selectItem`}
        onClick={handleClick}
    >
        <img 
            className='imgSelect' 
            src={data.img}
            alt={data.name} 
        />
        <span className='nameSelectItem'> {data.name} </span>
    </div>
  )
}
