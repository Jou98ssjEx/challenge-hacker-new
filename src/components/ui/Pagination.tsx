import { useContext } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';

import queryString from 'query-string';

import { QueryContext } from '../../context';
import { PaginationNumber } from './PaginationNumber';

import './PaginationModule.css';

/**
 * This component has three sections: next, previous and numbers.
 * of pages which is static
 */

let numberPage = 6;
// An array will be created to map the component
let arr = [...Array(numberPage)];

export const Pagination = () => {

    /**
     * From my context I have my function to change the page which has
     * as argument the number of the page in which it is located
     * for the navigation I use my hook, and to know where I am
     * I use the location another hook, I use a library to extract the query
     * and page of my url, so that I can specify to my component which data and styles to apply
     * and styles to apply
     */
    const { changePage } = useContext(QueryContext);
    
    const navigate = useNavigate();

    const location = useLocation();

    const { query, page } = queryString.parse(  location.search);

    const nPage = Number(page);

    /**
     * I create my function next page, in which I read the parameters
     * I read the parameters of the url and if it doesn't have a default, I make a validation
     * so that it can't continue to advance if the page it is on is smaller
     * to my page number
     */

    const onNextPage =( ) => {
        if(nPage < numberPage){
            const a = nPage +1;
            changePage(a.toString())
            navigate(`?query=${ !query ? 'all': query }&page=${page ? (nPage + 1): '1'}`);
        }
    }

    /**
     * I create my previous page function and do the same process as above.
     * validating in a different way
     */
    const onPrevPage = ( ) => {
        if(nPage > 1){
            const a = nPage -1;
            changePage(a.toString())
            navigate(`?query=${ !query ? 'all': query }&page=${page ? (nPage - 1): '1'}`);
        }
    }
    
  return (
    <>
        <div
            className='paginationContent'
        >
            {/* btnPrve */}
            <div className='rectanglePagination' onClick={onPrevPage} >
                <div className='contentIcon'>
                    <div className='iconPrev'></div>
                </div>
            </div>

            {/* Numbers */}
            {
                arr.map( (d , idx)=> (

                    <PaginationNumber 
                        key={idx}
                        pageNumber={idx}
                    />
                ))
            }
            {/* btnNext */}
            <div className='rectanglePagination' onClick={onNextPage} >
                <div className='contentIcon'>
                    <div className='iconNext'></div>
                </div>
            </div>
        </div>
    </>
  )
}
