import { FC, ReactNode } from "react"
import { Navbar } from "../ui";


/**
 * This component is a container, here you could define the title of the page with all the information required by the HEAD.
 * of the page with all the information required by the HEAD, here it is going to
 * pass all with child components that wrap around this layout
 * 
 * @param children
 */

interface Props {
  children: ReactNode;
}

export const MainLayout: FC<Props> = ({ children }) => {
  return (
    <>
      <Navbar />

      <main>
        { children }
      </main>

    </>
  )
}
