import { FavoriteProvider,  QueryProvider} from "./context"
import { AppRouter } from "./router"

/**
 *
 * This component is my highest point in the application,
 * It is where the context information will be provided.
 *  
 */

export const HackerNew = () => {
  return (
    <FavoriteProvider>
      <QueryProvider>
        <AppRouter />
      </QueryProvider>
    </FavoriteProvider>
  )
}
