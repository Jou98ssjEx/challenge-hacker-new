

/**
 * This function allows me to centralize the information
 * that will be required for filtering
 */

export interface ISelectItems {

    items: ISelectData[];

}

export interface ISelectData{
    name: optionQuery;
    img: string;
    classN: string;
}

export type optionQuery = 'Angular' | 'React' | 'Vuejs' | 'All';

 
export const selectItemData: ISelectItems = {
    items:[
        {
            name: 'Angular',
            img: '/assets/image-138.png',
            classN: '',
        },
        {
            name: 'React',
            img: '/assets/image-140.png',
            classN: '',
        },
        {
            name: 'Vuejs',
            img: '/assets/image-141.png',
            classN: '',
        },
    ]
}
