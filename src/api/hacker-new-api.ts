import axios from "axios";

/**
 * Denne funktion giver mig mulighed for at oprette basen for den URL, som jeg vil kalde for at hente oplysningerne
 * af den nyhed, der skal vises
 */

export const hackerApi = axios.create({
    baseURL: `https://hn.algolia.com/api/v1/search_by_date`,
    // baseURL: `https://hn.algolia.com/api/v1/search_by_date?query=angular&page=0`
});