import { useState, useEffect } from 'react';

import { MainLayout, Menu } from "../components"
import { AllScreen, MyFavesScreen } from "../screens"

/**
 * 
 * This component belongs to the main page
 * The layout and the components are defined in it
 * necessary for its visualization
 * 
 */

export const Home = () => {

  /**
   * I create a change of state with an effect to maintain
   * the view of the user's selection
   */

  const [changeScreen, setChangeScreen] = useState(true);
  useEffect(() => {
    const d: any = localStorage.getItem('menu');
      if (d){
        setChangeScreen(JSON.parse(d))
      }
  }, []);
  

  return (
    <>
        <MainLayout>

          <Menu 
            changeScreen={changeScreen} 
            setChangeScreen={setChangeScreen} 
          />
          {
            changeScreen ? <AllScreen /> : <MyFavesScreen /> 
          }

          

        </MainLayout>
    </>
  )
}
