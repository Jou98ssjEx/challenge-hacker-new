import { hackerApi } from "../api";
import { IData, IResponseAPI } from "../interfaces";

/**
 * 
 * This function calls the api and requires parameters such as the query and the page.
 * and the page, get the data, process what I need, 
 * I do some comparisons to define the favorites
 * 
 * @param query 
 * @param page 
 */


export const callEndPoint = async (query:string, page: string) => {

// the query is all lowercase
   query = query.toLowerCase();

// I make the request to the endpoint
    let { data } =  await hackerApi.get<IResponseAPI>(`?query=${query}&page=${page}`);

// Get my favorites if they exist in the localStora
    let favoitesArr:{ favorites: IData[]} = JSON.parse(localStorage.getItem('favorites')!) || [];

// I extract the information I need
    let newData = data.hits.map( (hit) => {

        // I make comparisons to return favorites
        for (let i = 0; i < favoitesArr.favorites.length; i++) {
            if( hit.objectID === favoitesArr.favorites[i].id){
                return{
                    id: hit.objectID,
                    author: hit.author,
                    story_title: hit.story_title,
                    story_url: hit.story_url,
                    created_at: hit.created_at,
                    isFavorite: true
                }
            } 
        }

        return{
            id: hit.objectID,
            author: hit.author,
            story_title: hit.story_title,
            story_url: hit.story_url,
            created_at: hit.created_at,
            isFavorite: false
        }
    })

// I make a comparison to filter out the ones without these fields
    let info = newData.filter( hit => !(hit.story_title === null && hit.story_url === null));

// return the necessary information
    return{
        info: info.splice(0, 8),
        query: data.query,
        page: data.page,
    }
    
}

