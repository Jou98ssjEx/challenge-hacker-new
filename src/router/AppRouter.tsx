import { Home } from '../page/Home';

import {BrowserRouter, Route, Routes} from 'react-router-dom'

/**
 *
 * This component is created to define routes, 
 * in this case to handle the url queries.
 *  
 */

export const AppRouter = () => {
  return (
    <BrowserRouter>
        <Routes>
             <Route path="/" element={<Home />} /> 
        </Routes>
    </BrowserRouter>

  )
}
