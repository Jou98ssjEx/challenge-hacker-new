import { CardHackerNew, Selector } from "../components"
import { Pagination } from "../components/ui/Pagination";
import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';


/**
 * 
 * This component will render all the information of the endpoint
 * endpoint, where it will be able to filter and have the pagination.
 * 
 */



export const AllScreen = () => {

  const navigate = useNavigate();
  
/**
 * This hook is activated the first time, to establish
 * if there is data in the local storage use that information.
 *
 */

  useEffect(() => {
    const state = JSON.parse(localStorage.getItem('state')!) || [];
    if(state){
      navigate(`/?query=${state.optionQuery.toLowerCase()}&page=${state.page}`)
    }
    
  }, [navigate]);



  return (
    <>
      <Selector />

      <CardHackerNew />

      <Pagination />
    </>
  )
}
