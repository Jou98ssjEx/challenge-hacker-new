import { CardHackerNew } from '../components'
import { useContext } from 'react';
import { FavoriteContext } from '../context/favorite/FavoriteContext';

/**
 *
 * This component displays all the favorites. 
 * 
 */

export const MyFavesScreen = () => {
  
  const {favorites} = useContext(FavoriteContext);


  return (
    <>
      <CardHackerNew favorites={favorites}/>
    </>
  )
}
