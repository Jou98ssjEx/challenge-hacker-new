import { IData } from '../interfaces/responseApi';
import { optionQuery } from '../utils';
import { useState, useEffect } from 'react';
import { callEndPoint } from '../helper/callEndPoint';


/**
 * This custom hook allows me to call my endpoint,
 * two parameters are required
 * 
 * @param query
 * @param page
 * 
 */

// I define initial properties
interface Props{
    cardNew: IData[];
    loading: boolean;
    error: string | null

    // data adicional
    query: string,
    page: number,
}

export const useHackerNew = ( query: optionQuery, page: string ) =>{

    const initialState: Props = {
        cardNew: [],
        error:null,
        loading: false,
        page: 1,
        query: '',
    }

    /**
     * To keep my information I opt for a change of status
     * by the time I ask for it again I already have the information ready
     */
    const [hackerNew, setHackerNew] = useState<Props>(initialState);

    // I opt for an effect that is pending change.
    useEffect(() => {
        // I call my endpoint
      callEndPoint(query, page)
        .then( resp => {
            // I have the answer and I apply it to my state
            setHackerNew({
                cardNew: resp.info,
                error: null,
                loading: false,
                page: resp.page,
                query: resp.query
            })
        })
        // if I have an error, I store my error
        .catch( error => {
            setHackerNew( d => ({...d, error}))
        });


    }, [ query, page]);


    // return the information
    return hackerNew;
    

}